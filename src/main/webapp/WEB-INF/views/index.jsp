<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
<head>
<title>T - SMAR</title>

<!-- Start CSS -->
<link rel="stylesheet" type="text/css" href="resources/stylesheets/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="resources/stylesheets/bootstrap.css">
<link rel="stylesheet" type="text/css" href="resources/stylesheets/bootflat.css">
<link rel="stylesheet" type="text/css" href="resources/stylesheets/bootflat-square.css">
<link rel="stylesheet" type="text/css" href="resources/stylesheets/jquery.fileupload.css">
<link rel="stylesheet" type="text/css" href="resources/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="resources/stylesheets/xeditable.css">
<link href="resources/stylesheets/kendo.common.min.css" rel="stylesheet" />
<link href="resources/stylesheets/kendo.default.min.css" rel="stylesheet" />
 
<!-- Fonts from Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="resources/stylesheets/app.css">
<!-- End CSS -->

</head>
<body>
	<div ng-view></div>

	<!-- Javascripts -->
	<script type="text/javascript" src="resources/javascripts/require.js" data-main="app/main.js"></script>
	<script src="http://cdn.sockjs.org/sockjs-0.3.min.js"></script>
	<script type="text/javascript" src="resources/javascripts/stomp.min.js"></script>
</body>
</html>