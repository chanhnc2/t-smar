//define([ '../module' ], function(controllers) {
//	'use strict';
//
//	controllers.controller('boardCtrl', function($rootScope, $scope, $http, $location, accountService, projectService) {
//		$scope.type = 1;
//		$scope.changeType = function (type) {
//			$scope.type = type;
//		};
//		
//		$scope.postComment = function () {
//			$scope.taskComment = '';
//		};
//		
//		$scope.columns = [{
//			modal: 'idea-modal',
//			title: 'Ideas',
//			tasks: [{
//				title: 'idea task',
//				showComment: false,
//				comments: ['comment']
//			}]
//		}, {
//			modal: 'todo-modal',
//			title: 'Todo',
//			tasks: [{
//				title: 'todo task',
//				showComment: false,
//				comments: ['comment']
//			}]
//		}, {
//			modal: 'in-progress-modal',
//			title: 'In progress',
//			tasks: [{
//				title: 'in progress task',
//				showComment: false,
//				comments: ['comment']
//			}]
//		}, {
//			modal: 'done-modal',
//			title: 'Done',
//			tasks: [{
//				title: 'done task',
//				showComment: false,
//				comments: ['comment']
//			}]
//		}, ];
//
//		
//		$scope.sortableOptions = {
//				placeholder: 'item',
//				connectWith: '.items-container'
//		};
//		
//		$scope.sortableOptions1 = {
//				placeholder: 'col',
//				connectWith: '.cols-container'
//		};
//		
//		$scope.addTask = function (listIndex) {
//			if ($scope.task.title != '') {
//				$scope.columns[listIndex].tasks.push({
//					title: $scope.task.title,
//					showComment: false,
//					comments: []
//				});
//				$scope.task.title = '';
//			}
//		};
//		
//		// chart
//		$scope.chartObject = {};
//
//	    $scope.onions = [
//	        {v: "Onions"},
//	        {v: 3},
//	    ];
//
//	    $scope.chartObject.data = {"cols": [
//	        {id: "t", label: "Topping", type: "string"},
//	        {id: "s", label: "Slices", type: "number"}
//	    ], "rows": [
//	        {c: [
//	            {v: "Mushrooms"},
//	            {v: 3},
//	        ]},
//	        {c: $scope.onions},
//	        {c: [
//	            {v: "Olives"},
//	            {v: 31}
//	        ]},
//	        {c: [
//	            {v: "Zucchini"},
//	            {v: 1},
//	        ]},
//	        {c: [
//	            {v: "Pepperoni"},
//	            {v: 2},
//	        ]}
//	    ]};
//
//
//	    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
//	    $scope.chartObject.options = {
//	        'title': 'How Much Pizza I Ate Last Night'
//	    };
//	    
//	    // Add task to project
//	    
//	});
//});