define([ '../module' ], function(controllers) {
	'use strict';

	controllers.controller('loginCtrl', function($rootScope, $scope, $http, $location, accountService) {
		// Prepare data
		$rootScope.data = {
			isLogged : false,
			account : {}
		};
		
		// Login
		$scope.login = function() {
			accountService.login({
				userName: $scope.account.userName,
				password: $scope.account.password
			}).success(function(data, status) {
				if (data.success) {
					console.log('Login successful');

					$rootScope.data.isLogged = true;
					$rootScope.data.account = data.result;
					console.log('data ', $rootScope.data);
					
					$location.path('/myprojects');
				} else {
					console.log('Login fail');
				}
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
	});
});