define([ '../module' ], function(controllers) {
	'use strict';

	controllers.controller('projectListCtrl', function($rootScope, $scope, $location, $http, accountService, projectService) {
		// Data
		$scope.data = accountService.data;
		
		$scope.count = [];
		
		//-- Start function --//
		// Load project
		var loadProjects = function () {
			projectService.loadProjects().success(function (data, status) {
				$scope.projects = data.result;
				
				$scope.projects.forEach(function (project) {
					var temp = [];
					project.taskTypes.forEach(function (taskType) {
						var count = 0;
						taskType.columnTypes.forEach(function (columnType) {
							count += columnType.tasks.length;
						});
						
						temp.push(count);
					});
					$scope.count.push(temp);
				});
				
				console.log('Projects', data.result);
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Create project
		$scope.createProject = function () {
			projectService.createProject({
				name: $scope.data.projectName
			}).success(function (data, status) {
				$scope.projects.push(data.result);
				var temp = [0, 0, 0, 0];
				$scope.count.push(temp);
				
				console.log('Create project ', data.result);
				
				//loadProjects();
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Logout
		$scope.logout = function () {
			accountService.logout().success(function (data, status) {
				$location.path('/');
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		$scope.setProjectId = function (projectId) {
			$rootScope.projectId = projectId;
			$location.path('/board');
		};
		//-- End function --//
		
		loadProjects();
	});
});