define([
        './module',
        // Attach services here
        './accountService',
        './projectService',
        './taskService',
        './commentService',
        './columnTypeService',
        './notificationService'
], function () {
	
});