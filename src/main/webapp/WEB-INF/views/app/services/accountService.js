define(['./module'], function (services) {
	'use strict';
	
	services.service('accountService', function ($http, $q, $timeout) {
		this.baseUrl = 'api/accounts';
		
		// Data
		this.data = {
			userName: '',
			notifications: [],
			nUnactivedNoti: 0
		};
		
		this.getAccountInfo = function () {
			var defer = $q.defer();
			var data = this.data;
			
			$http.post(this.baseUrl + '/loadaccount').success(function (r) {
				data.id = r.result.id;
				data.userName = r.result.userName;
				data.notifications = r.result.notifications;
				var tempN = 0;
				var len = r.result.notifications.length;
				for (var i = 0; i < len; i++) {
					var noti = r.result.notifications[i];
					if (!noti.activated) {
						tempN++;
					}
				}
				data.nUnactivedNoti = tempN;
				defer.resolve(data);
			});
			
			return defer.promises;
		};
		
		// Get accounts list
		this.getAccountsList = function () {
			return $http.get(this.baseUrl);
		};

		// Create account
		this.create = function (input) {
			return $http.post(this.baseUrl, input);
		};
		
		// Load account
		this.loadAccount = function () {
			return $http.post(this.baseUrl + '/loadaccount', {});
		};
		
		// Login
		this.login = function (input) {
			return $http.post(this.baseUrl + '/login', input);
		};
		
		// Logout
		this.logout = function () {
			return $http.post(this.baseUrl + '/logout');
		};
		
		// Create new project
		this.createProject = function (accountId, input) {
			return $http.post(this.baseUrl + '/createproject/' + accountId, input);
		};
		
		// Get rule
		this.getRule = function (accountProjectBody) {
			return $http.post(this.baseUrl + '/getrule', accountProjectBody);
		};
	});
});