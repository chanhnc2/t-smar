define(['./module'], function (services) {
	'use strict';
	
	services.service('commentService', function ($http) {
		this.baseUrl = 'api/comments/';
		
		// Add comment
		this.addComment = function (taskId, commentBody) {
			return $http.post(this.baseUrl + 'addcomment/' + taskId, commentBody);
		};
	});
});