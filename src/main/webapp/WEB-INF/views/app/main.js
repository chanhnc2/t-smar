/*host.com/js/...
host.com/stylesheet
v        /images
        /<controller>/<action>*/
require.config({
	paths: {
		'jquery': 'vendor/jquery',
		'jquery-ui': 'vendor/jquery-ui',
		'bootstrap': 'vendor/bootstrap',
		'angular': 'vendor/angular',
		'angular-route': 'vendor/angular-route',
		'angular-ui-bootstrap': 'vendor/angular-ui-bootstrap',
		'angular-sortable': 'vendor/angular-sortable',
		'ng-google-chart': 'vendor/ng-google-chart',
		'slidebox': 'vendor/slidebox',
		'kendo': 'vendor/kendo',
		'angular-kendo': 'vendor/angular-kendo',
		'xeditable': 'vendor/xeditable',
		'domReady': 'vendor/domReady'
	},
	shim: {
		'jquery': {
			exports: 'jquery'
		},
		'jquery-ui': {
			exports: 'jquery-ui',
			deps: ['jquery']
		},
		'bootstrap': {
			exports: 'bootstrap',
			deps: ['jquery']
		},
		'angular': {
			exports: 'angular',
			deps: ['jquery', 'jquery-ui']
		},
		'angular-route': {
			deps: ['angular']
		},
		'angular-ui-bootstrap': {
			deps: ['angular']
		},
		'angular-sortable': {
			deps: ['angular']
		},
		'ng-google-chart': {
			deps: ['angular']
		},
		'slidebox': {
			deps: ['angular']
		},
		'kendo': {
			deps: ['jquery']
		},
		'angular-kendo': {
			deps: ['angular']
		},
		'xeditable' : {
			deps: ['angular']
		}
	},
	deps: [
		'./start'
	]
});