/*tempalteCache.put "board.html", " html "


grunt*/

define(['app'], function (app) {
	'use strict';
	
	return app.config(function ($routeProvider) {
		$routeProvider.when('/', {
			controller: 'homeCtrl',
			templateUrl: 'app/views/frontend/home.html'
		}).when('/login', {
			controller: 'loginCtrl',
			templateUrl: 'app/views/frontend/login.html'
		}).when('/myprojects', {
			controller: 'projectListCtrl',
			templateUrl: 'app/views/frontend/projectList.html',
			resolve: {
				load: function (accountService) {
					return accountService.getAccountInfo();
				}
			}
		}).when('/board', {
			controller: 'boardCtrl',
			templateUrl: 'app/views/frontend/board.html',
			resolve: {
				load: function (accountService) {
					return accountService.getAccountInfo();
				}
			}
		}).when('/editboard', {
			controller: 'boardCtrl',
			templateUrl: 'app/views/frontend/editboard.html',
			resolve: {
				load: function (accountService) {
					return accountService.getAccountInfo();
				}
			}
		}).when('/analytic', {
			controller: 'boardCtrl',
			templateUrl: 'app/views/frontend/analytic.html',
			resolve: {
				load: function (accountService) {
					return accountService.getAccountInfo();
				}
			}
		}).otherwise({
			redirectTo: '/'
		});
	});
});