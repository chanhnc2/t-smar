/**
 * T-SMAR
 * Authors: Chanh - Tuyen
 * Thesis
 */

define([
        'angular',
        './controllers/index',
        './directives/index',
        './services/index',
        'angular-route',
        'angular-ui-bootstrap',
        'angular-sortable',
        'ng-google-chart',
        'slidebox',
        'angular-kendo',
        'xeditable'
], function (ng) {
	'use strict';
	
	return ng.module('app', [
	                         'app.controllers',
	                         'app.directives',
	                         'app.services',
	                         'ngRoute',
	                         'ui.bootstrap',
	                         'ui.sortable',
	                         'googlechart',
	                         'Slidebox',
	                         'kendo.directives',
	                         'xeditable'
	                         ]);
});