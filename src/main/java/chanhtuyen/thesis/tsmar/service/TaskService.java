package chanhtuyen.thesis.tsmar.service;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.Task;

public interface TaskService {
	public void create(Task task);
	public Task get(int id);
	public int getLatestIndex();
	public void update(Task task);
	public void delete(int id);
	public List<Task> getAll();
	public Task getLastTask();
}
