package chanhtuyen.thesis.tsmar.service;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.Comment;

public interface CommentService {
	public void create(Comment comment);
	public Comment get(int id);
	public void update(Comment comment);
	public void delete(int id);
	public List<Comment> getAll();
}
