package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.TaskDao;
import chanhtuyen.thesis.tsmar.model.Task;
import chanhtuyen.thesis.tsmar.service.TaskService;

public class TaskServiceImpl implements TaskService {
	
	@Autowired
	private TaskDao taskDao;

	@Override
	@Transactional
	public void create(Task task) {
		taskDao.create(task);
	}

	@Override
	@Transactional
	public Task get(int id) {
		return taskDao.get(id);
	}

	@Override
	@Transactional
	public void update(Task task) {
		taskDao.update(task);
	}

	@Override
	@Transactional
	public void delete(int id) {
		taskDao.delete(id);
	}

	@Override
	@Transactional
	public List<Task> getAll() {
		return taskDao.getAll();
	}

	@Override
	@Transactional
	public int getLatestIndex() {
		return taskDao.getLatestIndex();
	}

	@Override
	@Transactional
	public Task getLastTask() {
		return taskDao.getLastTask();
	}
}
