package chanhtuyen.thesis.tsmar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.CumulativeDao;
import chanhtuyen.thesis.tsmar.model.Cumulative;
import chanhtuyen.thesis.tsmar.service.CumulativeService;

public class CumulativeServiceImpl implements CumulativeService {
	
	@Autowired
	private CumulativeDao cumulativeDao;

	@Override
	@Transactional
	public void create(Cumulative c) {
		cumulativeDao.create(c);
	}

	@Override
	@Transactional
	public Cumulative get(int id) {
		return cumulativeDao.get(id);
	}

	@Override
	@Transactional
	public void update(Cumulative c) {
		cumulativeDao.update(c);
	}

}
