package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.CommentDao;
import chanhtuyen.thesis.tsmar.model.Comment;
import chanhtuyen.thesis.tsmar.service.CommentService;

public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDao commentDao;
	
	@Override
	@Transactional
	public void create(Comment comment) {
		commentDao.create(comment);	
	}

	@Override
	@Transactional
	public Comment get(int id) {
		return commentDao.get(id);
	}

	@Override
	@Transactional
	public void update(Comment comment) {
		commentDao.update(comment);
	}

	@Override
	@Transactional
	public void delete(int id) {
		commentDao.delete(id);
	}

	@Override
	@Transactional
	public List<Comment> getAll() {
		return commentDao.getAll();
	}
}
