package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.AccountDao;
import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.service.AccountService;


public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountDao accountDao;

	@Override
	@Transactional
	public void create(Account account) {
		accountDao.create(account);
	}

	@Override
	@Transactional
	public Account get(int id) {
		return accountDao.get(id);
	}

	@Override
	@Transactional
	public void update(Account account) {
		accountDao.update(account);
	}

	@Override
	@Transactional
	public void delete(int id) {
		accountDao.delete(id);
	}

	@Override
	@Transactional
	public List<Account> getAll() {
		return accountDao.getAll();
	}

	@Override
	@Transactional
	public Account login(String username, String password) {
		return accountDao.login(username, password);
	}

	@Override
	@Transactional
	public Account getByUserName(String userName) {
		return accountDao.getByUserName(userName);
	}
}
