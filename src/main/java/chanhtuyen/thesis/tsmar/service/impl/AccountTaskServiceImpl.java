package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.AccountTaskDao;
import chanhtuyen.thesis.tsmar.model.AccountTask;
import chanhtuyen.thesis.tsmar.service.AccountTaskService;

public class AccountTaskServiceImpl implements AccountTaskService {
	
	@Autowired
	private AccountTaskDao accountTaskDao;

	@Override
	@Transactional
	public void create(AccountTask accountTask) {
		accountTaskDao.create(accountTask);
	}

	@Override
	@Transactional
	public List<AccountTask> getMembers(int taskId) {
		return accountTaskDao.getMembers(taskId);
	}

	@Override
	@Transactional
	public List<AccountTask> getTask(int accountId) {
		return accountTaskDao.getTask(accountId);
	}

	@Override
	@Transactional
	public void delete(int accountId, int taskId) {
		accountTaskDao.delete(accountId, taskId);
	}

	@Override
	@Transactional
	public void setRule(AccountTask at, int rule) {
		accountTaskDao.setRule(at, rule);
	}

}
