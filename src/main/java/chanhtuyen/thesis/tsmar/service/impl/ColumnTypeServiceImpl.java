package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.ColumnTypeDao;
import chanhtuyen.thesis.tsmar.model.ColumnType;
import chanhtuyen.thesis.tsmar.service.ColumnTypeService;

public class ColumnTypeServiceImpl implements ColumnTypeService {
	
	@Autowired
	private ColumnTypeDao columnTypeDao;

	@Override
	@Transactional
	public void create(ColumnType columnType) {
		columnTypeDao.create(columnType);
	}

	@Override
	@Transactional
	public ColumnType get(int id) {
		return columnTypeDao.get(id);
	}

	@Override
	@Transactional
	public void update(ColumnType columnType) {
		columnTypeDao.update(columnType);
	}

	@Override
	@Transactional
	public void delete(int id) {
		columnTypeDao.delete(id);
	}

	@Override
	@Transactional
	public List<ColumnType> getAll() {
		return columnTypeDao.getAll();
	}

	@Override
	@Transactional
	public ColumnType getLast() {
		return columnTypeDao.getLast();
	}
}
