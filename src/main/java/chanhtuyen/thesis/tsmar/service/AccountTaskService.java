package chanhtuyen.thesis.tsmar.service;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.AccountTask;

public interface AccountTaskService {
	public void create(AccountTask accountTask);
	public List<AccountTask> getMembers(int taskId);
	public List<AccountTask> getTask(int accountId);
	public void delete(int accountId, int taskId);
	public void setRule(AccountTask at, int rule);
}
