package chanhtuyen.thesis.tsmar.service;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.Cumulative;
import chanhtuyen.thesis.tsmar.model.Project;

public interface ProjectService {
	public void create(Project project);
	public Project get(int id);
	public void update(Project project);
	public void delete(int id);
	public List<Project> getAll();
	public Project getLast();
	public Cumulative getLastCumulative(int projectId);
}
