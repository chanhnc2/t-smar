package chanhtuyen.thesis.tsmar.service;

import chanhtuyen.thesis.tsmar.model.Notification;

public interface NotificationService {
	public Notification get(int id);
	public void update(Notification notification);
}
