package chanhtuyen.thesis.tsmar.service;

import chanhtuyen.thesis.tsmar.model.Cumulative;

public interface CumulativeService {
	public void create(Cumulative c);
	public Cumulative get(int id);
	public void update(Cumulative c);
}
