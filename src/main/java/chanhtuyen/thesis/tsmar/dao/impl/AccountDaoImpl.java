package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.AccountDao;
import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.Project;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(Account account) {
		sessionFactory.getCurrentSession().save(account);
	}

	@Override
	public Account get(int id) {
		return (Account)sessionFactory.getCurrentSession().get(Account.class, id);
	}

	@Override
	public void update(Account account) {
		sessionFactory.getCurrentSession().update(account);
	}

	@Override
	public void delete(int id) {
		Account account = get(id);
		
		if (account != null) {
			sessionFactory.getCurrentSession().delete(account);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Account> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from Account").list();
	}

	@Override
	public Account login(String userName, String password) {
		Account account = null;
		String hql = "from Account a where a.userName = :userName and a.password = :password";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("userName", userName);
		query.setParameter("password", password);
		account = (Account)query.uniqueResult();
		
		return account;
	}

	@Override
	public Project createProject(Project project) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account getByUserName(String userName) {
		Account account = null;
		String hql = "from Account a where a.userName = :userName";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("userName", userName);
		account = (Account)query.uniqueResult();
		
		return account;
	}
}
