package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.TaskTypeDao;
import chanhtuyen.thesis.tsmar.model.TaskType;

@Repository
public class TaskTypeDaoImpl implements TaskTypeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(TaskType taskType) {
		sessionFactory.getCurrentSession().save(taskType);
	}

	@Override
	public TaskType get(int id) {
		return (TaskType)sessionFactory.getCurrentSession().get(TaskType.class, id);
	}

	@Override
	public void update(TaskType taskType) {
		sessionFactory.getCurrentSession().update(taskType);
	}

	@Override
	public void delete(int id) {
		TaskType taskType = get(id);
		
		if (taskType != null) {
			sessionFactory.getCurrentSession().delete(taskType);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskType> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from TaskType").list();
	}

}
