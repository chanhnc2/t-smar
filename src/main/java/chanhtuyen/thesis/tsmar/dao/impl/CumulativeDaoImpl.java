package chanhtuyen.thesis.tsmar.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.CumulativeDao;
import chanhtuyen.thesis.tsmar.model.Cumulative;

@Repository
public class CumulativeDaoImpl implements CumulativeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(Cumulative c) {
		sessionFactory.getCurrentSession().save(c);
	}

	@Override
	public void update(Cumulative c) {
		sessionFactory.getCurrentSession().update(c);
	}

	@Override
	public Cumulative get(int id) {
		return (Cumulative)sessionFactory.getCurrentSession().get(Cumulative.class, id);
	}
}
