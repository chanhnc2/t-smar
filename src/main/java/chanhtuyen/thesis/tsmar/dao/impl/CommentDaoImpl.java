package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.CommentDao;
import chanhtuyen.thesis.tsmar.model.Comment;

@Repository
public class CommentDaoImpl implements CommentDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(Comment comment) {
		sessionFactory.getCurrentSession().save(comment);
	}

	@Override
	public Comment get(int id) {
		return (Comment)sessionFactory.getCurrentSession().get(Comment.class, id);
	}

	@Override
	public void update(Comment comment) {
		sessionFactory.getCurrentSession().update(comment);
	}

	@Override
	public void delete(int id) {
		Comment comment = get(id);
		
		if (comment != null) {
			sessionFactory.getCurrentSession().delete(comment);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from Comment").list();
	}

}
