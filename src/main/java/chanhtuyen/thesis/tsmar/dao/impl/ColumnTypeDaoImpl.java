package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.ColumnTypeDao;
import chanhtuyen.thesis.tsmar.model.ColumnType;

@Repository
public class ColumnTypeDaoImpl implements ColumnTypeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(ColumnType columnType) {
		sessionFactory.getCurrentSession().save(columnType);
	}

	@Override
	public ColumnType get(int id) {
		return (ColumnType)sessionFactory.getCurrentSession().get(ColumnType.class, id);
	}

	@Override
	public void update(ColumnType columnType) {
		sessionFactory.getCurrentSession().update(columnType);
	}

	@Override
	public void delete(int id) {
		ColumnType columnType = get(id);
		
		if (columnType != null) {
			sessionFactory.getCurrentSession().delete(columnType);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ColumnType> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from ColumnType").list();
	}

	@Override
	public ColumnType getLast() {
		return getAll().get(getAll().size() - 1);
	}
}
