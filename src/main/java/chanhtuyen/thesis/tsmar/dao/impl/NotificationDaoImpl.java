package chanhtuyen.thesis.tsmar.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.NotificationDao;
import chanhtuyen.thesis.tsmar.model.Notification;

@Repository
public class NotificationDaoImpl implements NotificationDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Notification get(int id) {
		return (Notification)sessionFactory.getCurrentSession().get(Notification.class, id);
	}

	@Override
	public void update(Notification notification) {
		sessionFactory.getCurrentSession().update(notification);
	}
}
