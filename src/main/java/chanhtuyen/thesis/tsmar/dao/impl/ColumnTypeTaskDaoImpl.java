package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.ColumnTypeTaskDao;
import chanhtuyen.thesis.tsmar.model.ColumnTypeTask;

@Repository
public class ColumnTypeTaskDaoImpl implements ColumnTypeTaskDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ColumnTypeTask get(int columnTypeId, int taskId) {
		ColumnTypeTask columnTypeTask = null;
		String hql = "from ColumnTypeTask ctt where ctt.columnTypeId = :columnTypeId and ctt.taskId = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("columnTypeId", columnTypeId);
		query.setParameter("taskId", taskId);
		columnTypeTask = (ColumnTypeTask)query.uniqueResult();
		
		return columnTypeTask;
	}

	@Override
	public void update(int startColumnId, int targetColumnId, int taskId) {
		String hql = "update ColumnTypeTask ctt set ctt.columnTypeId = :targetColumnId"
				+ " where ctt.columnTypeId = :startColumnId and ctt.taskId = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("startColumnId", startColumnId);
		query.setParameter("targetColumnId", targetColumnId);
		query.setParameter("taskId", taskId);
		
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ColumnTypeTask> getList(int columnTypeId) {
		List<ColumnTypeTask> list = new ArrayList<ColumnTypeTask>();
		
		String hql = "from ColumnTypeTask ctt where ctt.columnTypeId = :columnTypeId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("columnTypeId", columnTypeId);
		list = query.list();
		
		return list;
	}

	@Override
	public void updateSort(int columnTypeId, int taskId, int newTaskId) {
		String hql = "update ColumnTypeTask ctt set ctt.taskId = :newTaskId"
				+ " where ctt.columnTypeId = :columnTypeId and ctt.taskId = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("columnTypeId", columnTypeId);
		query.setParameter("taskId", taskId);
		query.setParameter("newTaskId", newTaskId);
		
		query.executeUpdate();
	}

	@Override
	public void delete(int taskId) {
		String hql = "delete from ColumnTypeTask ctt where ctt.taskId = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("taskId", taskId);
		
		query.executeUpdate();
	}
}
