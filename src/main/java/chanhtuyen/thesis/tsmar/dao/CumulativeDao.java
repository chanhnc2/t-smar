package chanhtuyen.thesis.tsmar.dao;

import chanhtuyen.thesis.tsmar.model.Cumulative;

public interface CumulativeDao {
	public void create(Cumulative c);
	public Cumulative get(int id);
	public void update(Cumulative c);
}
