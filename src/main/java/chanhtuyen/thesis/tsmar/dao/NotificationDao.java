package chanhtuyen.thesis.tsmar.dao;

import chanhtuyen.thesis.tsmar.model.Notification;

public interface NotificationDao {
	public Notification get(int id);
	public void update(Notification notification);
}
