package chanhtuyen.thesis.tsmar.dao;

import chanhtuyen.thesis.tsmar.model.AccountProject;

public interface AccountProjectDao {
	public void create(AccountProject ap);
	public void delete(int accountId, int projectId);
	public void setRule(AccountProject ap, int rule);
	public AccountProject get(AccountProject ap);
}
