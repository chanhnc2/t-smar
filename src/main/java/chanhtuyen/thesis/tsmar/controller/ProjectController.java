package chanhtuyen.thesis.tsmar.controller;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.AccountProject;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Notification;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.model.TaskType;
import chanhtuyen.thesis.tsmar.service.AccountProjectService;
import chanhtuyen.thesis.tsmar.service.AccountService;
import chanhtuyen.thesis.tsmar.service.ProjectService;

@Controller
@RequestMapping("/api/projects")
public class ProjectController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired 
	private AccountService accountService;
	
	@Autowired 
	AccountProjectService accountProjectService;
	
	// Create project
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult createProject(@RequestBody Project project, HttpSession session) {
		JsonResult result = null;
		String userName = (String)session.getAttribute("userName");
		
		if (userName != null) {
			Account account = accountService.getByUserName(userName);
			
			// Create default task type
			TaskType t1 = new TaskType("Idea");
			/*ColumnType ct1 = new ColumnType("New");
			t1.getColumnTypes().add(ct1);*/
			
			TaskType t2 = new TaskType("Todo");
			/*ColumnType ct2 = new ColumnType("New");
			t2.getColumnTypes().add(ct2);*/
			
			TaskType t3 = new TaskType("In progress");
			/*ColumnType ct3 = new ColumnType("New");
			t3.getColumnTypes().add(ct3);*/
			
			TaskType t4 = new TaskType("Done");
			/*ColumnType ct4 = new ColumnType("New");
			t4.getColumnTypes().add(ct4);*/
			
			Set<TaskType> taskTypes = new LinkedHashSet<TaskType>();
			taskTypes.add(t1);
			taskTypes.add(t2);
			taskTypes.add(t3);
			taskTypes.add(t4);
			
			project.setTaskTypes(taskTypes);
			
			account.getProjects().add(project);
			accountService.update(account);
			accountProjectService.setRule(new AccountProject(account.getId(), projectService.getLast().getId()), 1);
			project.setAccounts(null);
			result = new JsonResult(true, project);
		}
		
		return result;
	}
	
	// Load projects of an account
	@RequestMapping(value="/loadprojects", method=RequestMethod.GET)
	@ResponseBody
	public JsonResult loadProjects(HttpSession session) {
		JsonResult result = null;
		
		String userName = (String)session.getAttribute("userName");
		session.removeAttribute("projectId");
		
		if (userName != null) {
			Account account = accountService.getByUserName(userName);
			logger.info("projects", account.getProjects());
			Set<Project> projects = account.getProjects();
			
			for (Project p : projects) {
				p.setAccounts(null);
			}
			
			result = new JsonResult(true, projects);
		}
		
		return result;
	}
	
	// Load project
	@RequestMapping(value="/loadproject", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult loadProject(HttpSession session, @RequestBody Project project) {
		JsonResult result = new JsonResult(false, null);
		
		if (session.getAttribute("userName") != null) {
			Integer id = (Integer)session.getAttribute("projectId");
			if (id == null) {
				session.setAttribute("projectId", project.getId());
				id = project.getId();
			}
			Project p = projectService.get(id);
			
			for (Account a: p.getAccounts()) {
				a.setNotifications(null);
				a.setProjects(null);
			}
			
			result.setSuccess(true);
			result.setResult(p);
			
			//session.removeAttribute("listener");
		}
		
		return result;
	}
	
	// Get members
	@RequestMapping(value="/getmembers", method=RequestMethod.GET)
	@ResponseBody
	public JsonResult getMembers(HttpSession session) {
		JsonResult result = new JsonResult(true, null);
		
		Integer projectId = (Integer)session.getAttribute("projectId");
		
		if (projectId != null) {
			Project project = projectService.get(projectId);
			
			for (Account a : project.getAccounts()) {
				a.setProjects(null);
				a.setNotifications(null);
			}
			
			result.setResult(project.getAccounts());
		}
		
		return result;
	}
	
	// Assign member
	@RequestMapping(value="/assigntoproject", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult assignMemberToProject(HttpSession session, @RequestBody Account account) {
		JsonResult result = new JsonResult(true, null);
		
		Account a = accountService.get(account.getId());
		Integer projectId = (Integer)session.getAttribute("projectId");
		AccountProject ap = new AccountProject();
		ap.setAccount_id(account.getId());
		ap.setProject_id(projectId);
		ap.setRule(3);
		accountProjectService.create(ap);
		Project project = projectService.get(projectId);
		
		String userName = (String)session.getAttribute("userName");
		Notification noti = new Notification();
		noti.setActivated(false);
		noti.setContent(userName + " assigned you to project " + project.getName());
		
		a.getNotifications().add(noti);
		accountService.update(a);
		projectService.update(project);
		a.setProjects(null);
		result.setResult(a);
		
		return result;
	}
	
	
	// Remove member
	@RequestMapping(value="/removemember/{accountId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult removeMember(HttpSession session, @PathVariable int accountId) {
		JsonResult result = new JsonResult(true, "id: " + accountId);
		int projectId = (Integer)session.getAttribute("projectId");
		accountProjectService.delete(accountId, projectId);
		
		return result;
	}
	
	@RequestMapping(value="/listener", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult listener(HttpSession session) {
		JsonResult result = new JsonResult(true, true);
		/*String userName = (String)session.getAttribute("userName");
		if (userName != null) {
			Account a = accountService.getByUserName(userName);
			if (!a.isSync()) {
				result.setResult(false);
				a.setSync(true);
				accountService.update(a);
			}
		}*/
		Boolean r = (Boolean)session.getAttribute("listen");
		if (!r) {
			result.setResult(false);
			session.setAttribute("listen", true);
		}
		
		return result;
	}
}