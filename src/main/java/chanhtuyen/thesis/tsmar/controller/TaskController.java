package chanhtuyen.thesis.tsmar.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.AccountTask;
import chanhtuyen.thesis.tsmar.model.BoardRequest;
import chanhtuyen.thesis.tsmar.model.ColumnType;
import chanhtuyen.thesis.tsmar.model.Cumulative;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Notification;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.model.Task;
import chanhtuyen.thesis.tsmar.model.TaskRequest;
import chanhtuyen.thesis.tsmar.service.AccountService;
import chanhtuyen.thesis.tsmar.service.AccountTaskService;
import chanhtuyen.thesis.tsmar.service.ColumnTypeService;
import chanhtuyen.thesis.tsmar.service.ColumnTypeTaskService;
import chanhtuyen.thesis.tsmar.service.CumulativeService;
import chanhtuyen.thesis.tsmar.service.ProjectService;
import chanhtuyen.thesis.tsmar.service.TaskService;
import chanhtuyen.thesis.tsmar.utils.Const;

@Controller
@RequestMapping("/api/tasks")
public class TaskController {
	
	@Autowired
	private SimpMessagingTemplate template;
	private Project resProject = new Project();
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ColumnTypeService columnTypeService;
	
	@Autowired
	private ColumnTypeTaskService columnTypeTaskService;

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private AccountTaskService accountTaskService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CumulativeService cumulativeService;
	
	private void updateResProject() {
		try {
			Integer id = (Integer)Const.session.getAttribute("projectId");
			if (id != null) {
				resProject = projectService.get(id);
				for (Account a: resProject.getAccounts()) {
					a.setNotifications(null);
					a.setProjects(null);
				}
				template.convertAndSend("/topic/project", resProject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@MessageMapping("/addTaskSock")
	public void addTaskSock(TaskRequest request) throws Exception {
		ColumnType columnType = columnTypeService.get(request.getColumnTypeId());
		Task task = new Task();
		task.setName(request.getName());
		task.setDescription(request.getDescription());
		task.setColorId(request.getColorId());
		task.setDueDate(request.getDueDate());
		task.setTindex(taskService.getLatestIndex() + 1);
		columnType.getTasks().add(task);
		columnTypeService.update(columnType);
		
		updateResProject();
	}
	
	// Add task
	@RequestMapping(value="/addtask/{columnTypeId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult addTask(HttpSession session,
			@PathVariable(value="columnTypeId") int columnTypeId,
			@RequestBody Task task) {
		
		JsonResult result = new JsonResult(true, null);
		ColumnType columnType = columnTypeService.get(columnTypeId);
		task.setTindex(taskService.getLatestIndex() + 1);
		columnType.getTasks().add(task);
		columnTypeService.update(columnType);
		
		result.setResult(taskService.getLastTask());
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	@MessageMapping("/updateBoardSock")
	public void updateBoardSock(BoardRequest boardRequest) throws Exception {
		if (boardRequest.getColumnStartId() != boardRequest.getColumnTargetId()) {
			ColumnType target = columnTypeService.get(boardRequest.getColumnTargetId());
			Task task = taskService.get(boardRequest.getTaskId());
			
			// Remove task from start column
			columnTypeTaskService.delete(boardRequest.getTaskId());
			
			// Add task to target column
			target.getTasks().add(task);
			columnTypeService.update(target);
			ColumnType target2 = columnTypeService.get(boardRequest.getColumnTargetId());
			int curPos = 0;
			int newPos = boardRequest.getTaskNewPos();
			int count = 0;
			for (Task t : target2.getTasks()) {
				if (t.getId() == boardRequest.getTaskId()) {
					curPos = count;
					break;
				}
				
				count++;
			}
			
			if (curPos != newPos) {
				count = 0;
				int tempIndex = -1;
				Task tempTask = null;
				if (curPos > newPos) {
					for (Task t : target2.getTasks()) {
						if (count > curPos) {
							break;
						}
						
						if (count == newPos) {
							tempTask = t;
						}
						
						if (count > newPos) {
							tempIndex = tempTask.getTindex();
							tempTask.setTindex(t.getTindex());
							t.setTindex(tempIndex);
							
							tempTask = t;
						}
						
						count++;
					}
				} else { // curPos < newPos
					for (Task t : target2.getTasks()) {
						/*if (count > newPos) {
							tempTask.setTindex(tempIndex);
							break;
						}*/
						
						if (count == curPos) {
							tempIndex = t.getTindex();
							tempTask = t;
						}
						
						if (count > curPos) {
							int temp = t.getTindex();
							t.setTindex(tempIndex);
							
							tempIndex = temp;
							
							if (count == newPos) {
								tempTask.setTindex(tempIndex);
								break;
							}
						}
						
						count++;
					}
				}
				
				columnTypeService.update(target2);
			}
			
			updateResProject();
		}
	}
	
	// Update board
	@RequestMapping(value="/updateboard", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateBoard(HttpSession session,
			@RequestBody BoardRequest boardRequest) {
		JsonResult result = new JsonResult(true, null);
		//session.setAttribute("listsener", "listen");
		
		if (boardRequest.getColumnStartId() != boardRequest.getColumnTargetId()) {
			ColumnType target = columnTypeService.get(boardRequest.getColumnTargetId());
			Task task = taskService.get(boardRequest.getTaskId());
			
			// Remove task from start column
			columnTypeTaskService.delete(boardRequest.getTaskId());
			
			// Add task to target column
			target.getTasks().add(task);
			columnTypeService.update(target);
			ColumnType target2 = columnTypeService.get(boardRequest.getColumnTargetId());
			int curPos = 0;
			int newPos = boardRequest.getTaskNewPos();
			int count = 0;
			for (Task t : target2.getTasks()) {
				if (t.getId() == boardRequest.getTaskId()) {
					curPos = count;
					break;
				}
				
				count++;
			}
			
			result.setResult(curPos + "-" + newPos);
			
			if (curPos != newPos) {
				count = 0;
				int tempIndex = -1;
				Task tempTask = null;
				if (curPos > newPos) {
					result.setResult("curPos > newPos");
					
					for (Task t : target2.getTasks()) {
						if (count > curPos) {
							break;
						}
						
						if (count == newPos) {
							tempTask = t;
						}
						
						if (count > newPos) {
							tempIndex = tempTask.getTindex();
							tempTask.setTindex(t.getTindex());
							t.setTindex(tempIndex);
							
							tempTask = t;
						}
						
						count++;
					}
				} else { // curPos < newPos
					result.setResult("curPos < newPos");
					
					for (Task t : target2.getTasks()) {
						/*if (count > newPos) {
							tempTask.setTindex(tempIndex);
							break;
						}*/
						
						if (count == curPos) {
							tempIndex = t.getTindex();
							tempTask = t;
						}
						
						if (count > curPos) {
							int temp = t.getTindex();
							t.setTindex(tempIndex);
							
							tempIndex = temp;
							
							if (count == newPos) {
								tempTask.setTindex(tempIndex);
								break;
							}
						}
						
						count++;
					}
				}
				
				columnTypeService.update(target2);
			}
		}
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	@RequestMapping(value="/updatename/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateTaskName(HttpSession session, @PathVariable int taskId, @RequestBody String nameTask) {
		JsonResult result = new JsonResult(true, nameTask);
		
		if (!nameTask.isEmpty()) {
			Task task = taskService.get(taskId);
			task.setName(nameTask);
			taskService.update(task);
			
			updateResProject();
		}
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	@RequestMapping(value="/updatedescription/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateTaskDescription(HttpSession session, @PathVariable int taskId, @RequestBody String descriptionTask) {
		JsonResult result = new JsonResult(true, descriptionTask);
		
		if (!descriptionTask.isEmpty()) {
			Task task = taskService.get(taskId);
			task.setDescription(descriptionTask);
			taskService.update(task);
			
			updateResProject();
		}
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	@RequestMapping(value="/updatecolor/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateTaskColor(HttpSession session, @PathVariable int taskId, @RequestBody int colorId) {
		JsonResult result = new JsonResult(true, "color");
		
		Task task = taskService.get(taskId);
		task.setColorId(colorId);
		taskService.update(task);
		
		updateResProject();
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);

		return result;
	}
	
	@RequestMapping(value="/updateduedate/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateTaskColor(HttpSession session, @PathVariable int taskId, @RequestBody String dueDate) {
		JsonResult result = new JsonResult(true, dueDate);
		String temp = dueDate.substring(1, dueDate.length() - 2);
		Task task = taskService.get(taskId);
		task.setDueDate(temp);
		taskService.update(task);
		
		updateResProject();
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);

		return result;
	}
	
	// Delete task
	@RequestMapping(value="/delete/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult delete(HttpSession session, @PathVariable int taskId) {
		JsonResult result = new JsonResult(true, "id: " + taskId);
		taskService.delete(taskId);
		
		updateResProject();
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	// Assign member to a task
	@RequestMapping(value="/assignmember", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult assignMember(HttpSession session, @RequestBody AccountTask accountTask) {
		JsonResult result = new JsonResult(true, accountTask);
		
		Task task = taskService.get(accountTask.getTask_id());
		Account account = accountService.get(accountTask.getAccount_id());
		accountTaskService.create(accountTask);
		
		String userName = (String)session.getAttribute("userName");
		Notification noti = new Notification();
		noti.setActivated(false);
		noti.setContent(userName + " assigned you to task " + task.getName());
		account.getNotifications().add(noti);
		accountService.update(account);
		
		account.setNotifications(null);
		account.setProjects(null);
		account.setTasks(null);
		result.setResult(account);
		
		return result;
	}
	
	// Remove member from a task
	@RequestMapping(value="/removemember", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult removeMember(HttpSession session, @RequestBody AccountTask accountTask) {
		JsonResult result = new JsonResult(true, accountTask);
		
		accountTaskService.delete(accountTask.getAccount_id(), accountTask.getTask_id());
		
		return result;
	}

	// Remove members of a task
	@RequestMapping(value = "/getmembers", method = RequestMethod.POST)
	@ResponseBody
	public JsonResult getMembers(HttpSession session, @RequestBody AccountTask accountTask) {
		JsonResult result = new JsonResult(true, "empty");

		List<AccountTask> list = accountTaskService.getMembers(accountTask.getTask_id());
		List<Account> accounts = new ArrayList<Account>();
		
		for (AccountTask at : list) {
			Account a = accountService.get(at.getAccount_id());
			a.setNotifications(null);
			a.setProjects(null);
			
			accounts.add(a);
		}
		
		result.setResult(accounts);

		return result;
	}
	
	@RequestMapping(value = "/statistic/{type}", method = RequestMethod.POST)
	@ResponseBody
	private JsonResult statistic(HttpSession session, @PathVariable(value="type") int type) {
		JsonResult result = new JsonResult(true, type);
		Integer pId = (Integer)session.getAttribute("projectId");
		Project p = projectService.get(pId);
		Cumulative cumulative = projectService.getLastCumulative(pId);
		
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		
		if (cumulative == null) {
			cumulative = new Cumulative();
			cumulative.setDate(String.valueOf(year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", day));
			switch (type) {
			case 0:
				cumulative.setnTodo(cumulative.getnTodo() + 1);
				break;
			case 1:
				cumulative.setnDoing(cumulative.getnDoing() + 1);
				break;
			case 2:
				cumulative.setnDone(cumulative.getnDone() + 1);
				break;
			case 3:
				cumulative.setnProduct(cumulative.getnProduct() + 1);
				break;
			default:
				break;
			}
			p.getCumulatives().add(cumulative);
			projectService.update(p);
		} else {
			String date = cumulative.getDate();
			int y = Integer.valueOf(date.substring(0, 4));
			int m = Integer.valueOf(date.substring(5, 7)) - 1;
			int d = Integer.valueOf(date.substring(8, 10));
			
			if (year == y && month == m && day == d) {
				switch (type) {
				case 0:
					cumulative.setnTodo(cumulative.getnTodo() + 1);
					break;
				case 1:
					cumulative.setnDoing(cumulative.getnDoing() + 1);
					break;
				case 2:
					cumulative.setnDone(cumulative.getnDone() + 1);
					break;
				case 3:
					cumulative.setnProduct(cumulative.getnProduct() + 1);
					break;
				default:
					break;
				}
				
				cumulativeService.update(cumulative);
			} else {
				Cumulative cumu = new Cumulative();
				cumu.setDate(String.valueOf(year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", day));
				switch (type) {
				case 0:
					cumu.setnTodo(cumulative.getnTodo() + 1);
					break;
				case 1:
					cumu.setnDoing(cumulative.getnDoing() + 1);
					break;
				case 2:
					cumu.setnDone(cumulative.getnDone() + 1);
					break;
				case 3:
					cumu.setnProduct(cumulative.getnProduct() + 1);
					break;
				default:
					break;
				}
				
				p.getCumulatives().add(cumu);
				projectService.update(p);
			}
		}
		
		return result;
	}
}