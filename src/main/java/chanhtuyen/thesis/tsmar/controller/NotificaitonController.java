package chanhtuyen.thesis.tsmar.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Notification;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.service.NotificationService;
import chanhtuyen.thesis.tsmar.service.ProjectService;
import chanhtuyen.thesis.tsmar.utils.Const;

@Controller
@RequestMapping(value="/api/notifications")
public class NotificaitonController {
	
	@Autowired
	private SimpMessagingTemplate template;
	private Project resProject = new Project();
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private NotificationService notificationService;
	
	private void updateResProject() {
		try {
			Integer id = (Integer)Const.session.getAttribute("projectId");
			if (id != null) {
				resProject = projectService.get(id);
				for (Account a: resProject.getAccounts()) {
					a.setNotifications(null);
					a.setProjects(null);
				}
				template.convertAndSend("/topic/project", resProject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/update/{notificationId}", method=RequestMethod.GET)
	@ResponseBody
	public JsonResult updateNotificationStatus(HttpSession session, 
			@PathVariable(value="notificationId") int id) {
		JsonResult result = new JsonResult(true, null);
		
		if (session.getAttribute("userName") != null) {
			Notification noti = notificationService.get(id);
			
			if (!noti.isActivated()) {
				noti.setActivated(true);
				notificationService.update(noti);
				
				result.setResult(noti);
			}
			
			updateResProject();
		}
		
		return result;
	}
}
