package chanhtuyen.thesis.tsmar.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.UploadedFile;
import chanhtuyen.thesis.tsmar.service.impl.FileValidator;



@Controller
@RequestMapping(value="/api/uploadfile")
public class UploadController {

	@Autowired
	FileValidator fileValidator;

//	@RequestMapping(method = RequestMethod.POST)
//	@ResponseBody
//	public ModelAndView getUploadForm(
//			@ModelAttribute("uploadedFile") UploadedFile uploadedFile,
//			BindingResult result) {
//		return new ModelAndView("uploadForm");
//	}

	@RequestMapping(value="/fileupload", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult fileUploaded(
			@ModelAttribute("uploadedFile") UploadedFile uploadedFile,
			BindingResult result) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		//JsonResult resultfile;
		MultipartFile file = uploadedFile.getFile();
		fileValidator.validate(uploadedFile, result);

		String fileName = file.getOriginalFilename();

		if (result.hasErrors()) {
			return  new JsonResult(false,"failed");
		}

		try {
			inputStream = file.getInputStream();

			File newFile = new File("C:/Users/chanhnguye.nc2/Desktop/SpringMVCForm/" + fileName);
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return new JsonResult(true, "success");
	}

}
