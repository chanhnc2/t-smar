package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ColumnNameRequest implements Serializable {
	
	private int columnTypeId;
	private String columnTypeName;
	
	public ColumnNameRequest() {
		
	}

	public int getColumnTypeId() {
		return columnTypeId;
	}

	public void setColumnTypeId(int columnTypeId) {
		this.columnTypeId = columnTypeId;
	}

	public String getColumnTypeName() {
		return columnTypeName;
	}

	public void setColumnTypeName(String columnTypeName) {
		this.columnTypeName = columnTypeName;
	}
}
