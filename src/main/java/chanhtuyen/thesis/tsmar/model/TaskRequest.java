package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;


@SuppressWarnings("serial")
public class TaskRequest implements Serializable {
	
	private int columnTypeId;
	private int id;
	private String name;
	private String description;
	private int tindex;
	private int colorId;
	private String dueDate;
	
	public TaskRequest() {
		
	}

	public int getColumnTypeId() {
		return columnTypeId;
	}

	public void setColumnTypeId(int columnTypeId) {
		this.columnTypeId = columnTypeId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTindex() {
		return tindex;
	}

	public void setTindex(int tindex) {
		this.tindex = tindex;
	}

	public int getColorId() {
		return colorId;
	}

	public void setColorId(int colorId) {
		this.colorId = colorId;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
}
