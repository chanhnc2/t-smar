package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="columntype_task")
public class ColumnTypeTask implements Serializable {
	
	@Id
	@Column(name="columntype_id")
	private int columnTypeId;
	
	@Id
	@Column(name="task_id")
	private int taskId;
	
	public ColumnTypeTask() {
		
	}

	public int getColumnTypeId() {
		return columnTypeId;
	}

	public void setColumnTypeId(int columnTypeId) {
		this.columnTypeId = columnTypeId;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
}