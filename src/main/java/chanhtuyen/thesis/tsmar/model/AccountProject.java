package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="account_project")
public class AccountProject implements Serializable {
	
	@Id
	@Column(name="account_id")
	private int account_id;
	
	@Id
	@Column(name="project_id")
	private int project_id;
	
	@Column(name="rule")
	private int rule;
	
	public AccountProject() {
		
	}
	
	public AccountProject(int accountId, int projectId) {
		this.account_id = accountId;
		this.project_id = projectId;
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public int getRule() {
		return rule;
	}

	public void setRule(int rule) {
		this.rule = rule;
	}
}
