package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="account_task")
public class AccountTask implements Serializable {
	
	@Id
	@Column(name="account_id")
	private int account_id;
	
	@Id
	@Column(name="task_id")
	private int task_id;
	
	public AccountTask() {
		
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public int getTask_id() {
		return task_id;
	}

	public void setTask_id(int task_id) {
		this.task_id = task_id;
	}
}
