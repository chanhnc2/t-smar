package chanhtuyen.thesis.tsmar.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="tasktype")
public class TaskType {
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@OneToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinTable(name="tasktype_columntype",
				joinColumns={@JoinColumn(name="tasktype_id")},
				inverseJoinColumns={@JoinColumn(name="columntype_id")})
	@OrderBy("id")
	private Set<ColumnType> columnTypes = new LinkedHashSet<ColumnType>();
	
	public TaskType() {
		
	}
	
	public TaskType(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ColumnType> getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(Set<ColumnType> columnTypes) {
		this.columnTypes = columnTypes;
	}
}
