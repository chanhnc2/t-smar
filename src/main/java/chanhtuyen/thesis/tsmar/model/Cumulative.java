package chanhtuyen.thesis.tsmar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cumulative")
public class Cumulative {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="date")
	private String date;
	
	@Column(name="nTodo")
	private int nTodo;
	
	@Column(name="nDoing")
	private int nDoing;
	
	@Column(name="nDone")
	private int nDone;
	
	@Column(name="nProduct")
	private int nProduct;
	
	public Cumulative() {
		nTodo = 0;
		nDoing = 0;
		nDone = 0;
		nProduct = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getnTodo() {
		return nTodo;
	}

	public void setnTodo(int nTodo) {
		this.nTodo = nTodo;
	}

	public int getnDoing() {
		return nDoing;
	}

	public void setnDoing(int nDoing) {
		this.nDoing = nDoing;
	}

	public int getnDone() {
		return nDone;
	}

	public void setnDone(int nDone) {
		this.nDone = nDone;
	}

	public int getnProduct() {
		return nProduct;
	}

	public void setnProduct(int nProduct) {
		this.nProduct = nProduct;
	}
}
